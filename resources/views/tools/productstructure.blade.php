@extends('template.app',['type'=>$tipe])
@section('title', 'Product Structure')
@section('content')
    <section class="container">
        <h3>Product Structure</h3>
        <ul>
            <li>IT Service</li>
            <ul>
                <li class='c'>Discount</li>
                <li>Discount OTC</li>
                <li>Rating</li>
                <table>
                    <thead>
                        <tr>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </ul>
        </ul>
        <div id='tabel'>
        <table border='1'>
            <thead>
                <tr>
                    <th>Col 1</th>
                    <th>Col 2</th>
                    <th>Col 3</th>
                    <th>Col 4</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Row 1</td>
                    <td>Row 1</td>
                    <td>Row 1</td>
                    <td>Row 1</td>
                </tr>
                <tr>
                    <td>Row 2</td>
                    <td>Row 2</td>
                    <td>Row 2</td>
                    <td>Row 2</td>
                </tr>
                <tr>
                    <td>Row 3</td>
                    <td>Row 3</td>
                    <td>Row 3</td>
                    <td>Row 3</td>
                </tr>
            </tbody>
        </table>
    </div>
    </section>

    <script type='text/javascript'>
    $(document).ready(function(){
        $('#tabel').hide();
    });

    $('.c').on('click',function(e){
        e.preventDefault();
        if($('#tabel').is(':visible')){
            $('#tabel').hide();
        }else{
            $('#tabel').show();
        }
    });
    </script>
@endsection