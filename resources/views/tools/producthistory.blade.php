@extends('template.app',['type'=>$tipe])
@section('title', 'Product History')
@section('content')
    <section class="container">
        <h3>Product History</h3>
        <table id="datatable" class="table table-hover">
            <thead>
                <tr>
                    <th>MODIFIED</th>
                    <th>MODIFIED BY</th>
                    <th>DESKRIPSI</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($items as $item)
                <tr>
                    <td>{{ $item['modified'] }}</td>
                    <td>{{ $item['modified_by'] }}</td>
                    <td>{{ $item['deskripsi'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@endsection