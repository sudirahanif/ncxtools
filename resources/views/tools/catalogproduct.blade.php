@extends('template.app',['type'=>$tipe])
@section('title', 'Catalog Product')
@section('content')
    <section class="container">
        <h3>Catalog Product</h3>
        <table id="datatable" class="table table-hover">
            <thead>
                <tr>
                    <th>PRODUCT</th>
                    <th>PART NUMBER</th>
                    <th>CATALOG NAME</th>
                    <th>PRODUCT CATEGORY</th>
                    @if($tipe=='ebis')
                        <th>USAGE BASED?</th>
                        <th>ACTION</th>
                    @endif
                </tr>
            </thead>
        </table>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            var table =  $('#datatable').DataTable({
                scrollX: true,
                processing: true,
                serverSide: true,
                @if($tipe=='ebis')
                ajax: '{{ route('ebis.getcatalogproduct') }}',
                @else
                ajax: '{{ route('wib.getcatalogproduct') }}',
                @endif
                columns: [
                    { data: 'product',name: 'product'},
                    { data: 'part_num',name: 'part_num'},
                    { data: 'catalogue_name',name: 'catalogue_name'},
                    { data: 'product_category',name: 'product_category'},
                    @if($tipe=='ebis')
                    { data: 'usage_based',name: 'usage_based'},
                    { data: 'product',name: 'product',render: function (data, type, full, meta) {
                                var productmodel = '{{ route("ebis.productmodel", ":id") }}';
                                var producthistory = '{{ route("ebis.producthistory", ":id") }}';
                                var productstructure = '{{ route("ebis.productstructure", ":id") }}';
                                var productsop = '{{ route("ebis.productsop", ":id") }}';
                                productmodel = productmodel.replace(':id', data);
                                producthistory = producthistory.replace(':id', data);
                                productstructure = productstructure.replace(':id', data);
                                productsop = productsop.replace(':id', data);
                                return '<a href="'+productmodel+'" data-popup="lightbox" target="_blank"> Model </a> | '+
                                '<a href="'+producthistory+'" data-popup="lightbox" target="_blank"> History </a> | '+
                                '<a href="'+productstructure+'" data-popup="lightbox" target="_blank"> Structure </a> | '+
                                '<a href="'+productsop+'" data-popup="lightbox" target="_blank"> SOP </a>';
                            }},
                    @endif
                ]
            });
        });
    </script>
@endsection