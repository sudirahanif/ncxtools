@if($list != null)
    @if($type === 'quote')
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
                <tr>
                    <td>ID QUOTE</td>
                    <td>CREATED</td>
                    <td>QUOTE NUM</td>
                    <td>STATUS</td>
                    <td>ACTIVE FLAG</td>
                    <td>SUBTYPE</td>
                    <td>ASSET INT ID</td>
                    <td>ACTION</td>
                </tr>
            </thead>
            <tbody>
            @foreach($list as $item)
                <tr>
                    <td>{{$item->row_id}}</td>
                    <td>{{$item->created}}</td>
                    <td>{{$item->quote_num}}</td>
                    <td>{{$item->stat_cd}}</td>
                    <td>{{$item->active_flg}}</td>
                    <td>{{$item->subtype_cd}}</td>
                    <td>{{$item->asset_int}}</td>
                    <td><a id="mandatory_attr" data-id="{{$item->quote_num}}" data-href="{{ route('ebis.mandatory_attr') }}" data-type="mandatory_attr" class="label">Mandatory Attr</a>&nbsp;</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @elseif($type === 'order')
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
                <tr>
                    <td>CREATED</td>
                    <td>ORDER NUM</td>
                    <td>STATUS CD</td>
                    <td>ACTIVE FLG</td>
                    <td>ROW ID</td>
                </tr>
            </thead>
            <tbody>
            @foreach($list as $item)
                <tr>
                    <td>{{$item->created}}</td>
                    <td>{{$item->order_num}}</td>
                    <td>{{$item->status_cd}}</td>
                    <td>{{$item->active_flg}}</td>
                    <td>{{$item->row_id}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@else
    <center><h4>Tidak Ditemukan</h4></center>
@endif