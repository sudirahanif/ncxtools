@if($list != null)
    @if($type_cari == 'account')
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
                <tr>
                    <td>ID ACC</td>
                    <td>SITE</td>
                    <td>ACC NAME</td>
                    <td>ACC TYPE</td>
                    <td>NIPNAS</td>
                    <td>ACCNAS</td>
                    <td>SEGMEN</td>
                    <td>PARENT ACC</td>
                    <td>PAR TYPE</td>
                    <td>CONTACT</td>
                    <td>LATITUDE</td>
                    <td>LONGITUDE</td>
                    <td>WP</td>
                    <td>ACTION</td>
                </tr>
            </thead>
            <tbody>
            @foreach($list as $item)
                <tr>
                    <td>{{$item->id_acc}}</td>
                    <td>{{$item->site}}</td>
                    <td>{{$item->acc_name}}</td>
                    <td>{{$item->acc_type}}</td>
                    <td>{{$item->nipnas}}</td>
                    <td>{{$item->accnas}}</td>
                    <td>{{$item->segmen}}</td>
                    <td>{{$item->parent_acc}}</td>
                    <td>{{$item->par_type}}</td>
                    <td>{{$item->contact}}</td>
                    <td>{{$item->latitude}}</td>
                    <td>{{$item->longitude}}</td>
                    <td>{{$item->wp}}</td>
                    <td width="150">&nbsp;
                        <a id="pemilik" data-id="{{ $item->id_acc }}" data-href="{{ route('ebis.pemilik') }}" data-type="account" class="label">Pemilik</a>&nbsp;
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @elseif($type_cari == 'sid')
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
                <tr>
                    <td>ID ASSET</td>
                    <td>INTEG ID</td>
                    <td>PRODUCT</td>
                    <td>SID</td>
                    <td>STATUS</td>
                    <td>AGREEMENT</td>
                    <td>REVISION</td>
                    <td>SA</td>
                    <td>ID SA</td>
                    <td>ACTION</td>
                </tr>
            </thead>
            <tbody>
            @foreach($list as $item)
                <tr>
                    <td>{{$item->id_asset}}</td>
                    <td>{{$item->integ_id}}</td>
                    <td>{{$item->product}}</td>
                    <td>{{$item->sid}}</td>
                    <td>{{$item->status}}</td>
                    <td>{{$item->agreement}}</td>
                    <td>{{$item->revision}}</td>
                    <td>{{$item->sa}}</td>
                    <td>{{$item->id_sa}}</td>
                    <td width="150">&nbsp;
                        <a id="pemilik" data-id="{{ $item->agree_id }}" data-href="{{ route('ebis.pemilik') }}" data-type="sid" class="label">Pemilik</a>&nbsp;
                        <a id="quote" data-id="{{ $item->integ_id }}" data-href="{{ route('ebis.quote') }}" data-type="quote" class="label">Quote</a>&nbsp;
                        <a id="order" data-id="{{ $item->integ_id }}" data-href="{{ route('ebis.order') }}" data-type="order" class="label">Order</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @elseif($type_cari == 'agreement')
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
                <tr>
                    <td>ID AGREE</td>
                    <td>AGREE NUM</td>
                    <td>AGREE NAME</td>
                    <td>STATUS</td>
                    <td>REV</td>
                    <td>TYPE</td>
                    <td>START DATE</td>
                    <td>END DATE</td>
                    <td>NUM PARENT</td>
                    <td>REV PARENT</td>
                    <td>ACTION</td>
                </tr>
            </thead>
            <tbody>
            @foreach($list as $item)
                <tr>
                    <td>{{$item->id_agree}}</td>
                    <td>{{$item->agree_num}}</td>
                    <td>{{$item->agree_name}}</td>
                    <td>{{$item->status}}</td>
                    <td>{{$item->rev}}</td>
                    <td>{{$item->type}}</td>
                    <td>{{$item->start_date}}</td>
                    <td>{{$item->end_date}}</td>
                    <td>{{$item->num_parent}}</td>
                    <td>{{$item->rev_parent}}</td>
                    <td width="150">&nbsp;
                        <a id="pemilik" data-id="{{ $item->id_agree }}" data-href="{{ route('ebis.pemilik') }}" data-type="agreement" class="label">Pemilik</a>&nbsp;
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif    
@else
    <center><h4>Tidak Ditemukan</h4></center>
@endif