@if($list != null)
    <table class="table table-bordered">
        <thead style="background-color: #bdc3c7">
            <tr>
                <td>ID POSTN</td>
                <td>POSITION</td>
                <td>USERNAME</td>
            </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>{{$item->id_postn}}</td>
                <td>{{$item->position}}</td>
                <td>{{$item->username}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <center><h4>Tidak Ditemukan</h4></center>
@endif