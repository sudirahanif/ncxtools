@if($oh)
    <div class="index">
        <div class="pricing">
            <ul>
                @if($tipe =='ebis')
                    <li class="unit">
                        <table class="table table-hover" style="text-align: left;">
                            <tr>
                                <td>ORDER NUM</td>
                                <td><strong>{{$oh[0]->ordernum}}</strong></td>
                            </tr>
                            <tr>
                                <td>ID ORDER</td>
                                <td><strong>{{$oh[0]->id_order}}</strong></td>
                            </tr>
                            <tr>
                                <td>ORDER STATUS</td>
                                <td><strong>{{$oh[0]->oh_status}}</strong></td>
                            </tr>
                            <tr>
                                <td>ORDER TYPE</td>
                                <td><strong>{{$oh[0]->order_type}}</strong></td>
                            </tr>
                            <tr>
                                <td>IS ACTIVE?</td>
                                <td><strong>{{$oh[0]->is_active}}</strong></td>
                            </tr>
                            <tr>
                                <td>REVISION</td>
                                <td><strong>{{$oh[0]->rev}}</strong></td>
                            </tr>
                            <tr>
                                <td>REASON</td>
                                <td><strong>{{$oh[0]->reason}}</strong></td>
                            </tr>
                        </table>
                    </li>
                    <li class="unit">
                        <table class="table table-hover" style="text-align: left;">
                            <tr>
                                <td>IS TERMIN?</td>
                                <td><strong>{{ $termin }}</strong></td>
                            </tr>
                            <tr>
                                <td>IS USAGE?</td>
                                <td><strong>{{ $usage }}</strong></td>
                            </tr>
                            <tr>
                                <td>AGREE NUM</td>
                                <td><strong>{{$oh[0]->agree}}</strong></td>
                            </tr>
                            <tr>
                                <td>QUOTE NUM</td>
                                <td><strong>{{$oh[0]->quote}}</strong></td>
                            </tr>
                            <tr>
                                <td>CREATED</td>
                                <td><strong>{{$oh[0]->created}}</strong></td>
                            </tr>
                            <tr>
                                <td>DUE</td>
                                <td><strong>{{$oh[0]->due}}</strong></td>
                            </tr>
                            <tr>
                                <td>CREATED BY</td>
                                <td><strong>{{$oh[0]->created_by}}</strong></td>
                            </tr>
                        </table>
                    </li>
                @else
                    <li class="unit">
                        <table class="table table-hover" style="text-align: left;">
                            <tr>
                                <td>ORDER NUM</td>
                                <td>{{$oh[0]->ordernum}}</td>
                            </tr>
                            <tr>
                                <td>ID ORDER</td>
                                <td>{{$oh[0]->id_order}}</td>
                            </tr>
                            <tr>
                                <td>ORDER STATUS</td>
                                <td>{{$oh[0]->oh_status}}</td>
                            </tr>
                            <tr>
                                <td>ORDER TYPE</td>
                                <td>{{$oh[0]->order_type}}</td>
                            </tr>
                            <tr>
                                <td>IS ACTIVE?</td>
                                <td>{{$oh[0]->is_active}}</td>
                            </tr>
                            <tr>
                                <td>REVISION</td>
                                <td>{{$oh[0]->rev}}</td>
                            </tr>
                            <tr>
                                <td>AGREE NUM</td>
                                <td>{{$oh[0]->agree}}</td>
                            </tr>
                            <tr>
                                <td>QUOTE NUM</td>
                                <td>{{$oh[0]->quote}}</td>
                            </tr>
                            <tr>
                                <td>CREATED</td>
                                <td>{{$oh[0]->created}}</td>
                            </tr>
                            <tr>
                                <td>DUE</td>
                                <td>{{$oh[0]->due}}</td>
                            </tr>
                        </table>
                    </li>
                @endif
            </ul>
        </div>
        <a id="history" data-id="{{$oh[0]->ordernum}}" data-href="{{ route('ebis.history') }}" class="label label-primary">History</a>&nbsp;
        <div class="blank"></div>
        <a id="owner" data-id="{{$oh[0]->id_order}}" data-href="{{ route('ebis.owner') }}" class="label label-primary">Owner</a>&nbsp;
        <div class="blank"></div>
        <a id="owner" data-id="{{$oh[0]->agree_id}}" data-href="{{ route('ebis.agreement') }}" class="label label-primary">Agreement</a>&nbsp;
        <div class="blank"></div>
    </div>
<div class="container">
    <center><h5>Order Line Item</h5></center>
    <div id="table-order">
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
            <tr>
                <th>ID OLI</th>
                <th>INT ID</th>
                <th>PRODUCT</th>
                <th>STATUS</th>
                <th>MILESTONE</th>
                <th>ACTION</th>
                <th>SID</th>
                <th>START</th>
                <th>NET</th>
                <th>OTC</th>
                <th>MRC</th>
                <th>PROVCOMP</th>
                <th>ACTION</th>
            </tr>
            </thead>
            <tbody>
                @foreach($li as $item)
                    @if($item->prod_type == 'Service Bundle')
                        <tr bgcolor="#f1c40f">
                            <td>{{$item->id_oli}}</td>
                            <td>{{$item->int_id}}</td>
                            <td>{{$item->product}}</td>
                            <td>{{$item->li_status}}</td>
                            <td>{{$item->milestone}}</td>
                            <td>{{$item->action}}</td>
                            <td>{{$item->sid_oli}}</td>
                            <td>{{$item->start_pri}}</td>
                            <td>{{$item->net_pri}}</td>
                            <td>{{$item->otc}}</td>
                            <td>{{$item->mrc}}</td>
                            <td>{{$item->provcomp}}</td>
                            <td width="150">&nbsp;
                                <a id="act" data-id="{{ $item->id_oli }}" data-href="{{ route('ebis.activity') }}" class="label">Activity</a>&nbsp;
                                <a id="attr" data-id="{{ $item->id_oli }}" data-href="{{ route('ebis.attribute') }}" class="label">Attribute</a>&nbsp;
                                <a id="ca" data-id="{{ $item->ca }}" data-href="{{ route('ebis.account') }}" class="label">CA</a>&nbsp;
                                <a id="ba" data-id="{{ $item->ba }}" data-href="{{ route('ebis.account') }}" class="label">BA</a>&nbsp;
                                <a id="sa" data-id="{{ $item->sa }}" data-href="{{ route('ebis.account') }}" class="label">SA</a>&nbsp;
                                <a id="prev_order" data-id="{{ $item->int_id }}" data-href="{{ route('ebis.prev_order') }}" class="label">Prev Order</a>&nbsp;
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td>{{$item->id_oli}}</td>
                            <td>{{$item->int_id}}</td>
                            <td>{{$item->product}}</td>
                            <td>{{$item->li_status}}</td>
                            <td>{{$item->milestone}}</td>
                            <td>{{$item->action}}</td>
                            <td>{{$item->sid_oli}}</td>
                            <td>{{$item->start_pri}}</td>
                            <td>{{$item->net_pri}}</td>
                            <td>{{$item->otc}}</td>
                            <td>{{$item->mrc}}</td>
                            <td>{{$item->provcomp}}</td>
                            <td width="150">&nbsp;
                                <a id="attr" data-id="{{ $item->id_oli }}" data-href="{{ route('ebis.attribute') }}" class="label">Attribute</a>&nbsp;
                                <a id="ca" data-id="{{ $item->ca }}" data-href="{{ route('ebis.account') }}" class="label">CA</a>&nbsp;
                                <a id="ba" data-id="{{ $item->ba }}" data-href="{{ route('ebis.account') }}" class="label">BA</a>&nbsp;
                                <a id="sa" data-id="{{ $item->sa }}" data-href="{{ route('ebis.account') }}" class="label">SA</a>&nbsp;
                            </td>
                        </tr>
                    @endif

                @endforeach
            </tbody>
        </table>
    </div>
    <div id="response2" style="overflow-y: auto;"></div>
</div>
@else
    <div class="index">
        <h3>Order Tidak Ditemukan</h3>
    </div>
@endif