@if($list != null)
    @if($type_line == 'history')
        <center><h5>{{ $title_line }}</h5></center>
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
            <tr>
                <th>ID ORDER</th>
                <th>ORDER NUM</th>
                <th>REV</th>
                <th>OH STATUS</th>
                <th>IS ACTIVE</th>
                <th>ORDER TYPE</th>
                <th>CREATED</th>
            </tr>
            </thead>
            <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{$item->id_order}}</td>
                        <td>{{$item->ordernum}}</td>
                        <td>{{$item->rev}}</td>
                        <td>{{$item->oh_status}}</td>
                        <td>{{$item->is_active}}</td>
                        <td>{{$item->order_type}}</td>
                        <td>{{$item->created}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @elseif($type_line == 'owner')
        <center><h5>{{ $title_line }}</h5></center>
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
            <tr>
                <th>ID POSTN</th>
                <th>POSITION</th>
                <th>USERNAME</th>
            </tr>
            </thead>
            <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{$item->id_postn}}</td>
                        <td>{{$item->position}}</td>
                        <td>{{$item->username}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @elseif($type_line == 'agreement')
        <center><h5>{{ $title_line }}</h5></center>
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
            <tr>
                <th>ID AGREE</th>
                <th>AGREE NUM</th>
                <th>AGREE NAME</th>
                <th>REV</th>
                <th>TYPE</th>
                <th>START DATE</th>
                <th>END DATE</th>
                <th>NUM PARENT</th>
                <th>REV PARENT</th>
            </tr>
            </thead>
            <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{$item->id_agree}}</td>
                        <td>{{$item->agree_num}}</td>
                        <td>{{$item->agree_name}}</td>
                        <td>{{$item->rev}}</td>
                        <td>{{$item->type}}</td>
                        <td>{{$item->start_date}}</td>
                        <td>{{$item->end_date}}</td>
                        <td>{{$item->num_parent}}</td>
                        <td>{{$item->rev_parent}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @elseif($type_line == 'attr')
        <center><h5>{{ $title_line }}</h5></center>
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
            <tr>
                <th>ID OLI</th>
                <th>ID ATTR</th>
                <th>NAME</th>
                <th>NUM VAL</th>
                <th>CHAR VAL</th>
                <th>ACTION</th>
            </tr>
            </thead>
            <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{$item->id_oli}}</td>
                        <td>{{$item->id_attr}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->num_val}}</td>
                        <td>{{$item->char_val}}</td>
                        <td>{{$item->action}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @elseif($type_line == 'act')
        <center><h5>{{ $title_line }}</h5></center>
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
            <tr>
                <th>ID ACT</th>
                <th>ID OLI</th>
                <th>ACTIVITY</th>
                <th>STATUS</th>
                <th>BSD</th>
                <th>CREATED</th>
            </tr>
            </thead>
            <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{$item->id_act}}</td>
                        <td>{{$item->id_oli}}</td>
                        <td>{{$item->activity}}</td>
                        <td>{{$item->status}}</td>
                        <td>{{$item->bsd}}</td>
                        <td>{{$item->created}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @elseif($type_line == 'prev_order')
        <center><h5>{{ $title_line }}</h5></center>
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
            <tr> 
                <th>LINK ASSET</th>
                <th>PRODUCT</th>
                <th>ORDER NUM</th>
                <th>REV</th>
                <th>ORDER TYPE</th>
                <th>OH STATUS</th>
                <th>ASSET STAT</th>
                <th>ORDER ACTIVE</th>
                <th>CREATED</th>
                <th>SID ASSET</th>
                <th>SID OLI</th>
            </tr>
            </thead>
            <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{$item->link_asset}}</td>
                        <td>{{$item->product}}</td>
                        <td>{{$item->ordernum}}</td>
                        <td>{{$item->rev}}</td>
                        <td>{{$item->order_type}}</td>
                        <td>{{$item->oh_status}}</td>
                        <td>{{$item->asset_stat}}</td>
                        <td>{{$item->order_active}}</td>
                        <td>{{$item->created}}</td>
                        <td>{{$item->sid_asset}}</td>
                        <td>{{$item->sid_oli}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <center><h5>{{ $title_line }}</h5></center>
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
            <tr>
                <th>ID ACC</th>
                <th>SITE</th>
                <th>ACC NAME</th>
                <th>ACC TYPE</th>
                <th>NIPNAS</th>
                <th>ACCNAS</th>
                <th>SEGMENT</th>
                <th>PARENT ACC</th>
                <th>PAR TYPE</th>
                <th>CONTACT</th>
                <th>LATITUDE</th>
                <th>LONGITUDE</th>
                <th>WP</th>
                <th>ADDR NAME</th>
            </tr>
            </thead>
            <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{$item->id_acc}}</td>
                        <td>{{$item->site}}</td>
                        <td>{{$item->acc_name}}</td>
                        <td>{{$item->acc_type}}</td>
                        <td>{{$item->nipnas}}</td>
                        <td>{{$item->accnas}}</td>
                        <td>{{$item->segmen}}</td>
                        <td>{{$item->parent_acc}}</td>
                        <td>{{$item->par_type}}</td>
                        <td>{{$item->contact}}</td>
                        <td>{{$item->latitude}}</td>
                        <td>{{$item->longitude}}</td>
                        <td>{{$item->wp}}</td>
                        <td>{{$item->addr_name}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@else
    <div class="index">
        <h3>Tidak Ditemukan</h3>
    </div>
@endif