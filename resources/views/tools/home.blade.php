@extends('template.app',['tipe'=>$tipe])
@section('title', 'Home')
@section('content')
    <div class="index">
        <h3>NCXTOOLS</h3>
        <p class="version-text">enter your order number</p>
        <p class="learn-more Bootflat">
        <center><input style="margin-bottom: 10px; width: 300px;line-height: 32px;padding: 20px 10px;" type="text" value=""
                       name="order" class="form-control" id="order" placeholder="Order Number" required></center>
        </p>
    </div>
    <div id="response"></div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#order").on('keyup', function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    var order = $("#order").val();
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type    : 'POST',
                        @if($tipe == 'ebis')
                        url     : '{{ route('ebis.getorderdetail') }}',
                        @else
                        url     : '{{ route('wib.getorderdetail') }}',
                        @endif
                        data    : {order: order},
                        beforeSend: function() {
                            $.preloader.start({
                                modal: true,
                                src : 'sprites2.png'
                            });
                        },
                        succes  : function () {
                            console.log('Sukses');
                        },
                        error   : function (xhr, status, error) {
                            console.log(xhr);
                            console.log(status);
                            console.log(error);
                        },
                        complete : function (result) {
                            $.preloader.stop();
                            $('#response').html(result.responseText);
                        }
                    });
                }
            });
        });

        $(document).on("click", "#history", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            action(id, 'history', href);
        });

        $(document).on("click", "#owner", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            action(id, 'owner', href);
        });

        $(document).on("click", "#agreement", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            action(id, 'agreement', href);
        });

        $(document).on("click", "#act", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            action(id, 'act', href);
        });

        $(document).on("click", "#attr", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            action(id, 'ca', href);
        });

        $(document).on("click", "#ca", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            action(id, 'ca', href);
        });

        $(document).on("click", "#sa", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            action(id, 'sa', href);
        });

        $(document).on("click", "#ba", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            action(id, 'ba', href);
        });

        $(document).on("click", "#prev_order", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            action(id, 'prev_order', href);
        });

        function action(id, type, href){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type    : 'POST',
                url     : href,
                data    : {id: id, type: type},
                beforeSend: function() {
                    $.preloader.start({
                        modal: true,
                        src : 'sprites2.png'
                    });
                    $('#response2').empty();
                },
                succes  : function () {
                    console.log('Sukses');
                },
                error   : function (xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                },
                complete : function (result) {
                    $.preloader.stop();
                    $('#response2').html(result.responseText);
                    $('#response2').hide();
                    $('#response2').fadeIn(1000);
                    $('html, body').animate({
                        scrollTop: $("#response2").offset().top
                    }, 500);
                }
            });
        }
    </script>
@endsection
