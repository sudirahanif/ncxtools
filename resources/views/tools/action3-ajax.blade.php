@if($list != null)
    @if($type === 'mandatory_attr')
        <table class="table table-bordered">
            <thead style="background-color: #bdc3c7">
                <tr>
                    <td>QUOTE NUM</td>
                    <td>PRODUCT NAME</td>
                    <td>DATA TYPE</td>
                    <td>PREV NUM</td>
                    <td>PREV CHAR</td>
                    <td>ATTRIBUTE NAME</td>
                    <td>CHAR VAL</td>
                    <td>NUM VAL</td>
                    <td>REQUIRED FLAG</td>
                </tr>
            </thead>
            <tbody>
            @foreach($list as $item)
                <tr>
                    <td>{{$item->QUOTE_NUM}}</td>
                    <td>{{$item->PRODUCT_NAME}}</td>
                    <td>{{$item->DATA_TYPE}}</td>
                    <td>{{$item->PREV_NUM}}</td>
                    <td>{{$item->PREV_CHAR}}</td>
                    <td>{{$item->ATTRIBUTE_NAME}}</td>
                    <td>{{$item->CHAR_VAL}}</td>
                    <td>{{$item->REQUIRED_FLAG}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@else
    <center><h4>Tidak Ditemukan</h4></center>
@endif