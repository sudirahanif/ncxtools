@extends('template.app',['tipe'=>$tipe])
@section('title', 'Pencarian')
@section('content')
    <div class="index">
        <h3>NCXTOOLS</h3>
        <p class="version-text">enter your account, SID or agree</p>
        <p class="learn-more Bootflat">
        <center><input style="margin-bottom: 10px; width: 300px;line-height: 32px;padding: 20px 10px;" type="text" value=""
                       name="order" class="form-control" id="id" placeholder="Search" required></center>
        </p>
        <label><input type="radio" name="type_cari" value="account"> Account </label>&nbsp;
        <label><input type="radio" name="type_cari" value="sid"> SID </label>&nbsp;
        <label><input type="radio" name="type_cari" value="agreement"> Agreement </label>&nbsp;
        {{--<input type="button" name="cari" value="Cari">--}}
    </div>
    <div class="container">
        <div id="response1"></div>
        <div id="response2"></div>
        <div id="response3"></div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            var radio_type_cari = '';

            $("#id").on('keyup', function (e) {
                if (e.keyCode == 13 && radio_type_cari != null) {
                    e.preventDefault();
                    var id = $("#id").val();
                    var type_cari = radio_type_cari;
                    cari(id, type_cari);
                }
            });

            $("input[name='cari']").on('click', function (e) {
                e.preventDefault();
                if($.trim($('#id').val()) != null && radio_type_cari != null){
                    var id = $("#id").val();
                    var type_cari = radio_type_cari;
                    cari(id, type_cari);
                }else{
                    alert('Kosong');
                }
            });

            $("input[name='type_cari']").on( "click", function(e){
                //e.preventDefault();
                radio_type_cari = $(this).val();
            });
        });

        function cari(id, type_cari){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type    : 'POST',
                @if($tipe == 'ebis')
                url     : '{{ route('ebis.cari') }}',
                @else
                url     : '{{ route('wib.getorderdetail') }}',
                @endif
                data    : {id:id, type_cari:type_cari},
                beforeSend: function() {
                    $.preloader.start({
                        modal: true,
                        src : 'sprites2.png'
                    });
                    $('#response1').empty();
                    $('#response2').empty();
                    $('#response3').empty();
                },
                succes  : function () {
                    console.log('Sukses');
                },
                error   : function (xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                },
                complete : function (result) {
                    $.preloader.stop();
                    $('#response1').html(result.responseText);
                    $('#response1').hide();
                    $('#response1').fadeIn(1000);
                    $('html, body').animate({
                        scrollTop: $("#response1").offset().top
                    }, 500);
                }
            });
        }

        $(document).on("click", "#pemilik", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            var type_pemilik = $(this).data('type');
            action(id, type_pemilik, href);
        });

        $(document).on("click", "#quote", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            var type = $(this).data('type');
            action(id, type, href);
        });

        $(document).on("click", "#order", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            var type = $(this).data('type');
            action(id, type, href);
        });

        $(document).on("click", "#mandatory_attr", function () {
            var href = $(this).data('href');
            var id = $(this).data('id');
            var type = $(this).data('type');
            action2(id, type, href);
        });

        function action(id, type, href){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type    : 'POST',
                url     : href,
                data    : {id: id, type: type},
                beforeSend: function() {
                    $.preloader.start({
                        modal: true,
                        src : 'sprites2.png'
                    });
                    $('#response2').empty();
                    $('#response3').empty();
                },
                succes  : function () {
                    console.log('Sukses');
                },
                error   : function (xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                },
                complete : function (result) {
                    $.preloader.stop();
                    $('#response2').html(result.responseText);
                    $('#response2').hide();
                    $('#response2').fadeIn(1000);
                    $('html, body').animate({
                        scrollTop: $("#response2").offset().top
                    }, 500);
                }
            });
        }

        function action2(id, type, href){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type    : 'POST',
                url     : href,
                data    : {id: id, type: type},
                beforeSend: function() {
                    $.preloader.start({
                        modal: true,
                        src : 'sprites2.png'
                    });
                    $('#response3').empty();
                },
                succes  : function () {
                    console.log('Sukses');
                },
                error   : function (xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                },
                complete : function (result) {
                    $.preloader.stop();
                    $('#response3').html(result.responseText);
                    $('#response3').hide();
                    $('#response3').fadeIn(1000);
                    $('html, body').animate({
                        scrollTop: $("#response3").offset().top
                    }, 500);
                }
            });
        }
    </script>
@endsection
