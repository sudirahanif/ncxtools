<!doctype html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <!-- CSS -->
    
    <link rel="stylesheet" href="{{ URL::asset('assets/css/site.min.css') }}">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    @yield('css')
    <!-- JS -->
    
    <script type="text/javascript" src="{{ URL::asset('assets/js/site.min.js') }}"></script>
    <script src="//code.jquery.com/jquery.js"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/jquery.preloaders.min.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
</head>
<body>
    <div class="docs-header header--noBackground">
        <nav class="navbar navbar-default navbar-custom" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @if($tipe == 'ebis')
                            <li><a class="nav-link" href="{{route('ebis.home')}}">Home</a></li>
                            <li><a class="nav-link" href="{{route('ebis.pencarian')}}">Pencarian</a></li>
                            <li><a class="nav-link" href="{{route('ebis.catalogproduct')}}">Catalog Product</a></li>
                        @else
                            <li><a class="nav-link" href="{{route('wib.home')}}">Home</a></li>
                            <li><a class="nav-link" href="{{route('wib.catalogproduct')}}">Catalog Product</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
        <!--footer-->
        <div class="site-footer">
            <div class="container">
                <div class="copyright clearfix">
                    <p><b>NCXTOOLS</b></p>
                    <p>&copy; 2017 MF improved by HS</p>
                </div>
            </div>
        </div>
    </div>
</body>
@yield('js')
</html>