<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class EbisController extends Controller{
    public function home(){
        return view('tools.home',['tipe'=>'ebis']);
    }

    public function getorderdetail(Request $request){
        $ordernum = $request->order;
        $tempoh = DB::connection('ebis')->select("
            SELECT t1.row_id           AS id_order, 
                   t1.order_num        AS ordernum, 
                   t1.rev_num          AS rev, 
                   t1.status_cd        AS oh_status, 
                   T1.active_flg       AS is_active, 
                   T2.attrib_05        AS order_type, 
                   To_char(t1.created + 7 / 24, 'dd-Mon-yyyy hh24:mi:ss')       AS created, 
                   To_char(t1.req_ship_dt + 7 / 24, 'dd-Mon-yyyy hh24:mi:ss')       AS due,  
                   t1.pr_postn_id      AS repk, 
                   t1.change_reason_cd AS reason,
                   t3.quote_num        AS quote,
                   t4.agree_num        AS agree,
                   t1.agree_id         AS agree_id,
                   t1.created_by       AS created_by 
            FROM   sblprd.s_order t1 
                   LEFT JOIN sblprd.s_order_x t2 
                          ON t2.par_row_id = t1.row_id
                   LEFT JOIN sblprd.s_doc_quote t3
                          ON t3.row_id = t1.quote_id
                   LEFT JOIN sblprd.s_doc_agree t4
                          ON t4.row_id = t1.agree_id 
            WHERE  t1.order_num = '$ordernum'
                   AND t1.rev_num = (SELECT Max(rev_num) 
                                     FROM   sblprd.s_order x 
                                     WHERE  x.order_num = t1.order_num 
                                            AND x.status_cd NOT IN ( 'Abandoned'))
        ");
        $oh = $li = false;
        if(!empty($tempoh)){
            $oh = $tempoh;
            $id_order = $oh[0]->id_order;

            $li = DB::connection('ebis')->select("
                SELECT t1.row_id                                                   AS id_oli, 
                       T1.serv_accnt_id                                            AS SA, 
                       T1.bill_accnt_id                                            AS BA, 
                       T1.owner_account_id                                         AS CA, 
                       t1.status_cd                                                AS li_status, 
                       T1.milestone_code                                           AS milestone, 
                       T1.asset_integ_id                                           AS int_Id, 
                       t2.NAME                                                     AS product, 
                       t1.action_cd                                                AS action, 
                       To_char(t1.created + 7 / 24, 'dd-mm-yyyy hh24:mi:ss')       AS created, 
                       T1.service_num                                              AS sid_oli, 
                       T2.billing_type_cd                                          AS prod_type, 
                       T1.adj_unit_pri                                             AS start_pri, 
                       T1.unit_pri                                                 AS net_pri, 
                       T1.onetime_chg_subtot                                       AS otc, 
                       T1.per_mth_chg_subtot                                       AS mrc, 
                       T1.agree_id                                                 AS id_agree, 
                       To_char(t1.req_ship_dt + 7 / 24, 'dd-Mon-yyyy hh24:mi:ss')  AS due, 
                       To_char(t1.completed_dt + 7 / 24, 'dd-Mon-yyyy hh24:mi:ss') AS provcomp,
                       t1.x_cx_termin_flg                                          AS termin,
                       t2.price_type_cd                                            AS price_type
                FROM   sblprd.s_order_item t1 
                       LEFT JOIN sblprd.s_prod_int t2 
                              ON t2.row_id = t1.prod_id 
                WHERE  t1.order_id = '$id_order' 
                ORDER  BY t1.created, 
                          t1.row_id
            ");

            $litem = $li;

            $termin = $usage = 'No';

            foreach ($litem as $item){
                if($item->prod_type === 'Service Bundle' && $item->termin === 'Y'){
                    $termin = 'Yes';
                    break;
                }
            }

            foreach ($litem as $item){
                if($item->price_type === 'Usage'){
                    $usage = 'Yes';
                    break;
                }
            }
        }
        return view('tools.order-ajax',['oh'=>$oh,'li'=>$li,'tipe'=>'ebis','termin'=>$termin, 'usage'=>$usage]);
    }

    public function getcatalogproduct(){
        $catalogproduct = DB::connection('ebis')->select("
            select 
                d.name          as product,
                d.part_num      as part_num,
                a.name          as catalogue_name,
                b.name          as product_category,
                (
                    select distinct price_type_cd 
                    from sblprd.s_prod_int x 
                    where x.part_num like regexp_substr(d.part_num,'[^-]+', 1)||'%' 
                    and x.price_type_cd = 'Usage'
                ) as usage_based
            from sblprd.S_CTLG a
                left join sblprd.S_CTLG_CAT b on b.ctlg_id = a.row_id
                left join sblprd.S_CTLG_CAT_prod c on c.ctlg_cat_id = b.row_id
                left join sblprd.s_prod_int d on d.row_id = c.prod_id
            where CTLG_TYPE_CD is not null and CTLG_TYPE_CD = 'Buying' 
                and d.name is not null
            order by d.created asc
         ");

        return Datatables::of($catalogproduct)->make(true);
    }

    public function attribute(Request $request){
        $li = 0;

        $li = DB::connection('ebis')->select("
        SELECT
            t2.order_item_id        as id_oli,
            t2.row_id               as id_attr,
            t2.attr_name            as name,
            t2.num_val              as num_val,
            t2.char_val             as char_val,
            t2.action_cd            as action 
        from
            sblprd.s_order_item t1 
            left join
            SBLPRD.S_ORDER_ITEM_XA t2 
            on T2.ORDER_ITEM_ID = t1.row_id 
        where
            t1.row_id = '$request->id'
            and
            hidden_flg = 'N' 
        order by
            T2.DATA_TYPE_CD
        ");

        $title_line = "Attributes Line Item ".$request->id;

        return view('tools.action-ajax',['title_line'=>$title_line, 'type_line'=>'attr', 'list'=>$li]);
    }

    public function prev_order(Request $request){
        $li = 0;

        $li = DB::connection('ebis')->select("
            SELECT
                t2.integration_id AS link_asset, 
                t5.NAME           AS product, 
                t3.order_num      AS ordernum, 
                t3.rev_num        AS rev, 
                t4.attrib_05      AS ORDER_type, 
                t3.status_cd      AS oh_status, 
                T2.status_cd      AS asset_stat, 
                T3.active_flg     AS order_active, 
                t3.created        AS created, 
                t2.serial_num     AS SID_asset, 
                t1.service_num    AS SID_oli 
            FROM   sblprd.s_order_item t1 
                LEFT JOIN sblprd.s_asset t2 
                        ON t2.integration_id = t1.asset_integ_id 
                LEFT JOIN sblprd.s_order t3 
                        ON t3.row_id = t1.order_id 
                LEFT JOIN sblprd.s_order_x t4 
                        ON t4.par_row_id = t3.row_id 
                LEFT JOIN sblprd.s_prod_int t5 
                        ON t5.row_id = t1.prod_id 
            WHERE  t1.asset_integ_id = '$request->id' 
            ORDER  BY T3.created DESC
        ");

        $title_line = "Prev Order Line Item ".$request->id;

        return view('tools.action-ajax',['title_line'=>$title_line, 'type_line'=>'prev_order', 'list'=>$li]);
    }

    public function history(Request $request){
        $li = 0;

        $li = DB::connection('ebis')->select("
            SELECT
                t1.row_id       as id_order,
                t1.order_num    as ordernum,
                t1.rev_num      as rev,
                t1.status_cd    as oh_status,
                T1.ACTIVE_FLG   as is_active,
                T2.ATTRIB_05    as order_type,
                t1.created      as created 
            from
                sblprd.s_order t1 
                left join
                    sblprd.s_order_x t2 
                    on t2.par_row_id = t1.row_id 
            where
                t1.order_num = '$request->id' 
            order by
                t1.created desc
        ");

        $title_line = "History Line Item ".$request->id;

        return view('tools.action-ajax',['title_line'=>$title_line, 'type_line'=>'history', 'list'=>$li]);
    }

    public function owner(Request $request){
        $li = 0;

        $li = DB::connection('ebis')->select("
            SELECT
                t1.row_id   as id_postn,
                T1.NAME     as position,
                t2.login    as username 
            from
                sblprd.s_order_postn t3 
                left join
                    sblprd.s_postn t1 
                    on t1.row_id = T3.POSTN_ID 
                left join
                    sblprd.s_user t2 
                    on t2.row_id = T1.PR_EMP_ID 
            where
                T3.ORDER_ID = '1-1D7L6Z9'
        ");

        $title_line = "Owner Line Item ".$request->id;

        return view('tools.action-ajax',['title_line'=>$title_line, 'type_line'=>'owner', 'list'=>$li]);
    }

    public function agreement(Request $request){
        $li = 0;

        $li = DB::connection('ebis')->select("
            SELECT
                t1.row_id           as id_agree,
                T1.AGREE_NUM        as agree_num,
                t1.name             as agree_name,
                T1.REV_NUM          as rev,
                T1.AGREE_TYPE_CD    as type,
                T1.EFF_START_DT     as start_date,
                t1.eff_end_dt       as end_date,
                T2.agree_num        as num_parent,
                T2.rev_num          as rev_parent 
            from
                sblprd.s_doc_agree t1 
                left join
                    sblprd.s_doc_agree t2 
                    on t2.row_id = t1.par_agree_id 
            where
                t1.row_id = '$request->id'
        ");

        $title_line = "Agreement Line Item ".$request->id;

        return view('tools.action-ajax',['title_line'=>$title_line, 'type_line'=>'agreement', 'list'=>$li]);
    }

    public function activity(Request $request){
        $li = 0;

        $li = DB::connection('ebis')->select("
        SELECT
            t1.row_id           as id_act,
            t1.order_item_id    as id_oli,
            t1.todo_cd          as activity,
            t1.evt_stat_cd      as status,
            T1.X_BILL_STRT_DT   as bsd,
            t1.created          as created 
        from
            sblprd.s_evt_act t1 
        where
            T1.order_item_id = '$request->id' 
        order by
            t1.created desc
        ");

        $title_line = "Activity Line Item ".$request->id;

        return view('tools.action-ajax',['title_line'=>$title_line, 'type_line'=>'act', 'list'=>$li]);
    }

    public function account(Request $request){
        $li = 0;

        $li = DB::connection('ebis')->select("
            SELECT
                t1.row_id           as id_acc,
                t1.loc              as site,
                t1.name             as acc_name,
                T1.ACCNT_TYPE_CD    as acc_type,
                T1.X_PTI1__NIPNAS   as nipnas,
                T1.X_PTI1_ACCNT_NAS as accnas,
                T1.X_PTI1_SEGMENT   as segmen,
                t2.loc              as parent_acc,
                T2.ACCNT_TYPE_CD    as par_type,
                T3.LAST_NAME        as contact,
                T4.LATITUDE         as latitude,
                T4.LONGITUDE        as longitude,
                T3.WORK_PH_NUM      as wp,
                t4.addr_name        as addr_name
            from
            sblprd.s_org_ext t1 
            left join
                SBLPRD.S_ORG_EXT t2 
                on t2.row_id = T1.PAR_OU_ID 
            left join
                SBLPRD.S_CONTACT t3 
                on t3.row_id = T1.PR_CON_ID 
            left join
                SBLPRD.S_ADDR_PER t4 
                on t4.row_id = T1.PR_ADDR_ID 
            where
            t1.row_id = '$request->id'
        ");

        $title_line = '';
        switch($request->type){
            case "ca":
                $title_line = "CA Line Item ".$request->id;
                break;
            case "sa":
                $title_line = "SA Line Item ".$request->id;
                break;
            case "ba":
                $title_line = "BA Line Item ".$request->id;
                break;
            default :
                $title_line = "";
            break;
        }

        return view('tools.action-ajax',['title_line'=>$title_line, 'type_line'=>'account', 'list'=>$li]);
    }

    public function pencarian(){
        return view('tools.pencarian',['tipe'=>'ebis']);
    }

    public function cari(Request $request){
        $li = 0;

        if($request->type_cari === 'account'){
            $li = DB::connection('ebis')->select("
                SELECT
                    t1.row_id               as id_acc,
                    t1.loc                  as site,
                    t1.name                 as acc_name,
                    T1.ACCNT_TYPE_CD        as acc_type,
                    T1.X_PTI1__NIPNAS       as nipnas,
                    T1.X_PTI1_ACCNT_NAS     as accnas,
                    T1.X_PTI1_SEGMENT       as segmen,
                    t2.loc                  as parent_acc,
                    T2.ACCNT_TYPE_CD        as par_type,
                    T3.LAST_NAME            as contact,
                    T4.LATITUDE             as latitude,
                    T4.LONGITUDE            as longitude,
                    T3.WORK_PH_NUM          as wp 
                from
                    sblprd.s_org_ext t1 
                    left join
                    SBLPRD.S_ORG_EXT t2 
                    on t2.row_id = T1.PAR_OU_ID 
                    left join
                    SBLPRD.S_CONTACT t3 
                    on t3.row_id = T1.PR_CON_ID 
                    left join
                    SBLPRD.S_ADDR_PER t4 
                    on t4.row_id = T1.PR_ADDR_ID 
                where
                    t1.loc = '$request->id'
            ");
        }else if($request->type_cari === 'sid'){
            $li = DB::connection('ebis')->select("
                SELECT
                    t1.row_id           as id_asset,
                    t1.integration_id   as integ_id,
                    t2.name             as product,
                    t1.serial_num       as sid,
                    T1.STATUS_CD        as status,
                    t3.agree_num        as agreement,
                    T3.REV_NUM          as revision,
                    t4.loc              as sa,
                    t4.row_id           as id_sa,
                    t3.row_id           as agree_id 
                from
                    sblprd.s_asset t1 
                    left join
                    sblprd.s_prod_int t2 
                    on t2.row_id = t1.prod_id 
                    left join
                    sblprd.s_doc_agree t3 
                    on t3.row_id = T1.CUR_AGREE_ID 
                    left join
                    sblprd.s_org_ext t4 
                    on t4.row_id = T1.SERV_ACCT_ID 
                where
                    T1.SERIAL_NUM = '$request->id'
            ");
        }else if($request->type_cari === 'agreement'){
            $li = DB::connection('ebis')->select("
                SELECT
                    t1.row_id           as id_agree,
                    T1.AGREE_NUM        as agree_num,
                    t1.name             as agree_name,
                    T1.STAT_CD          as status,
                    T1.REV_NUM          as rev,
                    T1.AGREE_TYPE_CD    as type,
                    T1.EFF_START_DT     as start_date,
                    t1.eff_end_dt       as end_date,
                    T2.agree_num        as num_parent,
                    T2.rev_num          as rev_parent 
                from
                    sblprd.s_doc_agree t1 
                    left join
                    sblprd.s_doc_agree t2 
                    on t2.row_id = t1.par_agree_id 
                where
                    t1.agree_num = '$request->id' 
                    or t1.name = '$request->id'
            ");
        }
        
        return view('tools.pencarian-ajax',['tipe'=>'ebis', 'list'=>$li, 'type_cari'=>$request->type_cari]);
    }

    public function pemilik(Request $request){
        $li = 0;

        if($request->type === 'account'){
            $li = DB::connection('ebis')->select("
                SELECT
                    t1.row_id   as id_postn,
                    T1.NAME     as position,
                    t2.login    as username 
                from
                    sblprd.s_accnt_postn t3 
                    left join
                    sblprd.s_postn t1 
                    on t1.row_id = T3.POSITION_ID 
                    left join
                    sblprd.s_user t2 
                    on t2.row_id = T1.PR_EMP_ID 
                where
                    T3.OU_ext_id = '$request->id'
            ");
        }else if($request->type === 'sid'){
            $li = DB::connection('ebis')->select("
                SELECT
                    t1.row_id   as id_postn,
                    T1.NAME     as position,
                    t2.login    as username 
                from
                    sblprd.s_agree_postn t3 
                    left join
                    sblprd.s_postn t1 
                    on t1.row_id = T3.POSTN_ID 
                    left join
                    sblprd.s_user t2 
                    on t2.row_id = T1.PR_EMP_ID 
                where
                    T3.Agree_ID = '$request->id'
            ");
        }else if($request->type === 'agreement'){
            $li = DB::connection('ebis')->select("
                SELECT
                    t1.row_id   as id_postn,
                    T1.NAME     as position,
                    t2.login    as username 
                from
                    sblprd.s_agree_postn t3 
                    left join
                    sblprd.s_postn t1 
                    on t1.row_id = T3.POSTN_ID 
                    left join
                    sblprd.s_user t2 
                    on t2.row_id = T1.PR_EMP_ID 
                where
                    T3.Agree_ID = '$request->id'
            ");
        }
        
        return view('tools.pemilik-ajax',['tipe'=>'ebis', 'list'=>$li]);
    }

    public function quote(Request $request){
        $li = 0;

        $li = DB::connection('ebis')->select("
            SELECT
                t2.created          as created,
                T2.QUOTE_NUM        as quote_num,
                T2.STAT_CD          as stat_cd,
                T2.ACTIVE_FLG       as active_flg,
                T2.ROW_ID           as row_id,
                T2.subtype_cd       as subtype_cd,
                t1.asset_integ_id   as asset_int 
            from
                sblprd.s_quote_item t1 
                left join
                sblprd.s_doc_quote t2 
                on t2.row_id = t1.sd_id 
            where
                T1.asset_integ_id = '$request->id' 
            order by
                t2.created desc
        ");
        
        return view('tools.action2-ajax',['tipe'=>'ebis', 'list'=>$li, 'type'=>$request->type]);
    }

    public function order(Request $request){
        $li = 0;

        $li = DB::connection('ebis')->select("
            SELECT
                t2.created      as created,
                T2.ORDER_NUM    as order_num,
                T2.STATUS_CD    as status_cd,
                T2.ACTIVE_FLG   as active_flg,
                T2.ROW_ID       as row_id 
            from
                sblprd.s_order_item t1 
                left join
                sblprd.s_order t2 
                on t2.row_id = t1.order_id 
            where
                T1.asset_integ_id = '$request->id' 
            order by
                t2.created desc
        ");
        
        return view('tools.action2-ajax',['tipe'=>'ebis', 'list'=>$li, 'type'=>$request->type]);
    }

    public function mandatory_attr(Request $request){
        $li = 0;

        $li = DB::connection('ebis')->select("
            SELECT
                T2.QUOTE_NUM        AS QUOTE_NUM,
                T4.NAME             AS PRODUCT_NAME,
                T3.DATA_TYPE_CD     AS DATA_TYPE,
                T3.PREV_NUM_VAL     AS PREV_NUM,
                T3.PREV_CHAR_VAL    AS PREV_CHAR,
                T3.ATTR_NAME        AS ATTRIBUTE_NAME,
                T3.CHAR_VAL         AS CHAR_VAL,
                T3.NUM_VAL          AS NUM_VAL,
                T3.REQUIRED_FLG     AS REQUIRED_FLAG 
            FROM
                SBLPRD.S_QUOTE_ITEM T1 
            LEFT JOIN
                SBLPRD.S_DOC_QUOTE T2 
                ON T2.ROW_ID = T1.SD_ID 
            LEFT JOIN
                SBLPRD.S_QUOTE_ITEM_XA T3 
                ON T3.QUOTE_ITEM_ID = T1.ROW_ID 
            LEFT JOIN
                SBLPRD.S_PROD_INT T4 
                ON T4.ROW_ID = T1.PROD_ID 
            WHERE
                T2.QUOTE_NUM = '$request->id' 
                AND T3.REQUIRED_FLG = 'Y' 
                AND 
                (
                (T3.DATA_TYPE_CD = 'TEXT' 
                    AND T3.CHAR_VAL IS NULL) 
                    OR 
                    (
                        T3.DATA_TYPE_CD IN 
                        (
                            'Number',
                            'Integer'
                        )
                        AND T3.NUM_VAL IS NULL
                    )
                )
        ");
        
        return view('tools.action3-ajax',['tipe'=>'ebis', 'list'=>$li, 'type'=>$request->type]);
    }

    public function test(){
        $li = DB::connection('ebis')->select("
            SELECT
                t1.row_id   as id_postn,
                T1.NAME     as position,
                t2.login    as username 
            from
                sblprd.s_agree_postn t3 
                left join
                sblprd.s_postn t1 
                on t1.row_id = T3.POSTN_ID 
                left join
                sblprd.s_user t2 
                on t2.row_id = T1.PR_EMP_ID 
            where
                T3.Agree_ID = '1-17W6CJT'
        ");

        return json_encode($li);
    }

    public function catalogproduct(){
        return view('tools.catalogproduct',['tipe'=>'ebis']);
    }

    public function productmodel($id){
        //return view('tools.productmodel',['data'=>$id]);
    }

    public function producthistory($id){
        $items[0] = array('modified' => '21-11-2018', 'modified_by' => 'Tony', 'deskripsi' => 'Tak ada');
        $items[1] = array('modified' => '22-11-2018', 'modified_by' => 'Deni', 'deskripsi' => 'Tak ada');
        $items[2] = array('modified' => '23-11-2018', 'modified_by' => 'Edi', 'deskripsi' => 'Tak ada');
        $items[3] = array('modified' => '24-11-2018', 'modified_by' => 'Wawan', 'deskripsi' => 'Tak ada');
        $items[4] = array('modified' => '25-11-2018', 'modified_by' => 'Ismail', 'deskripsi' => 'Tak ada'); 
        return view('tools.producthistory',['tipe'=>'ebis', 'items'=>$items]);
    }

    public function productstructure($id){
        return view('tools.productstructure',['tipe'=>'ebis', 'product'=>$id]);
    }

    public function productsop($id){
        return view('tools.productstructure',['tipe'=>'ebis', 'product'=>$id]);
    }
}
