<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class WibController extends Controller
{
    public function home(){
        return view('tools.home',['tipe'=>'wib']);
    }

    public function getcatalogproduct(){
        $catalogproduct = DB::connection('wib')->select("
            select 
                d.name as product,
                d.part_num as part_num,
                a.name as catalogue_name,
                b.name as product_category
            from siebel.S_CTLG a
            left join siebel.S_CTLG_CAT b on b.ctlg_id = a.row_id
            left join siebel.S_CTLG_CAT_prod c on c.ctlg_cat_id = b.row_id
            left join siebel.s_prod_int d on d.row_id = c.prod_id
            where CTLG_TYPE_CD is not null and CTLG_TYPE_CD = 'Buying' and a.name = 'Telkom Products'
            and d.name is not null
            order by d.created asc
         ");

        return Datatables::of($catalogproduct)->make(true);
    }

    public function catalogproduct(){
        return view('tools.catalogproduct',['tipe'=>'wib']);
    }

    public function getorderdetail(Request $request){
        $ordernum = $request->order;

        $tempoh = DB::connection('wib')->select("
            SELECT t1.row_id                                                  AS id_order, 
                   t1.order_num                                               AS ordernum, 
                   t1.rev_num                                                 AS rev, 
                   t1.status_cd                                               AS oh_status, 
                   T1.active_flg                                              AS is_active, 
                   T2.attrib_05                                               AS order_type, 
                   To_char(t1.created + 7 / 24, 'dd-Mon-yyyy hh24:mi:ss')     AS created, 
                   To_char(t1.req_ship_dt + 7 / 24, 'dd-Mon-yyyy hh24:mi:ss') AS due, 
                   t1.pr_postn_id                                             AS repk, 
                   t1.change_reason_cd                                        AS reason, 
                   t3.quote_num                                               AS quote, 
                   t4.agree_num                                               AS agree 
            FROM   siebel.s_order t1 
                   LEFT JOIN siebel.s_order_x t2 
                          ON t2.par_row_id = t1.row_id 
                   LEFT JOIN siebel.s_doc_quote t3 
                          ON t3.row_id = t1.quote_id 
                   LEFT JOIN siebel.s_doc_agree t4 
                          ON t4.row_id = t1.agree_id 
            WHERE  t1.order_num = '$ordernum' 
                   AND t1.rev_num = (SELECT Max(rev_num) 
                                     FROM   siebel.s_order x 
                                     WHERE  x.order_num = t1.order_num 
                                            AND x.status_cd NOT IN ( 'Abandoned' ))
        ");
        $oh = $li = false;
        if(!empty($tempoh)){
            $oh = $tempoh;
            $id_order = $oh[0]->id_order;

            $li = DB::connection('wib')->select("
                SELECT t1.row_id                                                   AS id_oli, 
                       T1.serv_accnt_id                                            AS SA, 
                       T1.bill_accnt_id                                            AS BA, 
                       T1.owner_account_id                                         AS CA, 
                       t1.status_cd                                                AS li_status, 
                       T1.milestone_code                                           AS milestone, 
                       T1.asset_integ_id                                           AS int_Id, 
                       t2.NAME                                                     AS product, 
                       t1.action_cd                                                AS action, 
                       To_char(t1.created + 7 / 24, 'dd-mm-yyyy hh24:mi:ss')       AS created, 
                       T1.service_num                                              AS sid_oli, 
                       T2.billing_type_cd                                          AS prod_type, 
                       T1.adj_unit_pri                                             AS start_pri, 
                       T1.unit_pri                                                 AS net_pri, 
                       T1.onetime_chg_subtot                                       AS otc, 
                       T1.per_mth_chg_subtot                                       AS mrc, 
                       T1.agree_id                                                 AS id_agree, 
                       To_char(t1.req_ship_dt + 7 / 24, 'dd-Mon-yyyy hh24:mi:ss')  AS due, 
                       To_char(t1.completed_dt + 7 / 24, 'dd-Mon-yyyy hh24:mi:ss') AS provcomp 
                FROM   siebel.s_order_item t1 
                       LEFT JOIN siebel.s_prod_int t2 
                              ON t2.row_id = t1.prod_id 
                WHERE  t1.order_id = '$id_order' 
                ORDER  BY t1.created, 
                          t1.row_id");
        }
        return view('tools.order-ajax',['oh'=>$oh,'li'=>$li,'tipe'=>'wib']);#,'termin'=>$termin]);
    }


}
