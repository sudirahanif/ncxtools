<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'ebis'], function () {
    Route::get('home', [
        'as'        => 'ebis.home',
        'uses'      => 'EbisController@home'
    ]);

    Route::post('getorder', [
        'as'        => 'ebis.getorderdetail',
        'uses'      => 'EbisController@getorderdetail'
    ]);

    Route::get('catalogproduct', [
        'as'        => 'ebis.catalogproduct',
        'uses'      => 'EbisController@catalogproduct'
    ]);

    Route::get('getcatalogproduct', [
        'as'        => 'ebis.getcatalogproduct',
        'uses'      => 'EbisController@getcatalogproduct'
    ]);

    Route::get('productmodel/{id}', [
        'as'        => 'ebis.productmodel',
        'uses'      => 'EbisController@productmodel'
    ]);

    Route::get('producthistory/{id}', [
        'as'        => 'ebis.producthistory',
        'uses'      => 'EbisController@producthistory'
    ]);

    Route::get('productstructure/{id}', [
        'as'        => 'ebis.productstructure',
        'uses'      => 'EbisController@productstructure'
    ]);

    Route::get('productsop/{id}', [
        'as'        => 'ebis.productsop',
        'uses'      => 'EbisController@productsop'
    ]);

    Route::post('history', [
        'as'        => 'ebis.history',
        'uses'      => 'EbisController@history'
    ]);

    Route::post('owner', [
        'as'        => 'ebis.owner',
        'uses'      => 'EbisController@owner'
    ]);

    Route::post('agreement', [
        'as'        => 'ebis.agreement',
        'uses'      => 'EbisController@agreement'
    ]);

    Route::post('activity', [
        'as'        => 'ebis.activity',
        'uses'      => 'EbisController@activity'
    ]);

    Route::post('attribute', [
        'as'        => 'ebis.attribute',
        'uses'      => 'EbisController@attribute'
    ]);

    Route::post('account', [
        'as'        => 'ebis.account',
        'uses'      => 'EbisController@account'
    ]);

    Route::post('prev_order', [
        'as'        => 'ebis.prev_order',
        'uses'      => 'EbisController@prev_order'
    ]);

    Route::get('pencarian', [
        'as'        => 'ebis.pencarian',
        'uses'      => 'EbisController@pencarian'
    ]);

    Route::post('cari', [
        'as'        => 'ebis.cari',
        'uses'      => 'EbisController@cari'
    ]);

    Route::post('pemilik', [
        'as'        => 'ebis.pemilik',
        'uses'      => 'EbisController@pemilik'
    ]);

    Route::post('quote', [
        'as'        => 'ebis.quote',
        'uses'      => 'EbisController@quote'
    ]);

    Route::post('order', [
        'as'        => 'ebis.order',
        'uses'      => 'EbisController@order'
    ]);

    Route::post('mandatory_attr', [
        'as'        => 'ebis.mandatory_attr',
        'uses'      => 'EbisController@mandatory_attr'
    ]);

    Route::get('test', [
        'as'        => 'ebis.test',
        'uses'      => 'EbisController@test'
    ]);
});

Route::group(['prefix' => 'wib'], function () {
    Route::get('home', [
        'as'        => 'wib.home',
        'uses'      => 'WibController@home'
    ]);

    Route::get('catalogproduct', [
        'as'        => 'wib.catalogproduct',
        'uses'      => 'WibController@catalogproduct'
    ]);

    Route::get('getcatalogproduct', [
        'as'        => 'wib.getcatalogproduct',
        'uses'      => 'WibController@getcatalogproduct'
    ]);

    Route::post('getorder', [
        'as'        => 'wib.getorderdetail',
        'uses'      => 'WibController@getorderdetail'
    ]);
});